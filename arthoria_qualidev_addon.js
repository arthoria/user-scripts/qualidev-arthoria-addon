// ==UserScript==
// @name        Arthoria QualiDev Addon
// @namespace   arthoria
// @description	Dieses Tool erweitert das Browsergame Arthoria um einige Funktionen und Möglichkeite, sowie Einstellungsmöglichkeiten und Rätsellöser
// @include     https://arthoria.de*
// @version		8.6
// @require		https://arthoria.srv1-14119.srv-net.de/arthoria_qualidev_addon/js/aes.js
// @require		https://arthoria.srv1-14119.srv-net.de/arthoria_qualidev_addon/js/sha1.js
// @grant       none
// ==/UserScript==

(function() {
    function gmFixWrapper() {
        if(!unsafeWindow)
            var unsafeWindow = window;
        var host = 'https://arthoria.de',
            addonServer = 'https://arthoria.srv1-14119.srv-net.de/arthoria_qualidev_addon/',
            $ = unsafeWindow.$,
            version=8.6,
            app;
        $.getScript('https://arthoria.srv1-14119.srv-net.de/arthoria_qualidev_addon/js/aes.js');
        $.getScript('https://arthoria.srv1-14119.srv-net.de/arthoria_qualidev_addon/js/sha1.js');
        var CONSTANTS = {
            LAYOUT:{
                GREEN:1,
                SCHRIFTROLLE:2
            },
            MENU:{
                DROPDOWN:1,
                NORMAL:2
            },
            CHAT:{
                TOP:1,
                RIGHT:2
            }
        };
        var environment = {
            userlayout:($('link').filter(function() {return $(this).attr('href')=='stylen.css';}).size()) ? CONSTANTS.LAYOUT.GREEN : CONSTANTS.LAYOUT.SCHRIFTROLLE,
            menutype:$('#mencat1').size() ? CONSTANTS.MENU.DROPDOWN : CONSTANTS.MENU.NORMAL,
            chatposition:$('#chat:has(.chattop)').size() ? CONSTANTS.CHAT.TOP : CONSTANTS.CHAT.RIGHT,
            puzzle:document.querySelector('#chat') ? false : true
        };
        Number.prototype.pad = function(count, char) {
            var numLength = (this+'').length;
            count = count || numLength;
            char = char || '0';
            return count>numLength?char.repeat(count-numLength)+this:this;
        }
        Date.prototype.getFullMonth = function() {
            return (this.getMonth()+1).pad(2);
        }
        Date.prototype.getFullDate = function() {
            return this.getDate().pad(2);
        }
        Date.prototype.getFullHours = function() {
            return this.getHours().pad(2);
        }
        Date.prototype.getFullMinutes = function() {
            return this.getMinutes().pad(2);
        }
        Date.prototype.getFullSeconds = function() {
            return this.getSeconds().pad(2);
        }

        Date.prototype.format = function(formatStr) {
            formatStr = formatStr || 'd.m.Y H:i:s';
            var replacements = {
                H:'getFullHours',
                i:'getFullMinutes',
                s:'getFullSeconds',
                Y:'getFullYear',
                m:'getFullMonth',
                d:'getFullDate'
            };
            return formatStr.replace(/(H|i|s|Y|m|d)/g, function(pat) { return Date.prototype[replacements[pat]].call(this); }.bind(this));
        }

        if(window.location.href.substring(0, host.length).toLowerCase() == host && unsafeWindow.charID) {
            // Application starten / ausführen:
            app = new ArthoriaAddon();
            app.addPlugin(new DandronSolverPlugin());
            //app.addPlugin(new ForenAccessPlugin());
            app.addPlugin(new ItemExtensionPlugin());
            app.addPlugin(new KrondallSolverPlugin());
            app.addPlugin(new PlayerExtendPlugin());
            app.addPlugin(new ResearchSolverPlugin());
            if(!environment.puzzle) {
                app.addPlugin(new StatusPagePlugin());
                app.addPlugin(new AlchimistHelperPlugin());
                app.addPlugin(new BattleExtendPlugin());
                app.addPlugin(new ChatPlugin());
                app.addPlugin(new SidebarPlugin());
                app.addPlugin(new PhasenportalPlugin());
            }
            // app.addPlugin(new WikiPlugin());
            app.init();
            //app.getPlugin('SidebarPlugin').addLink(1, 'Juwelenortung', 'http://arthoria.de/index.php?p=library&page=spells&use=58');
        }

        function ArthoriaAddon() {
// Variables:
            var self=this;
            this.cryptedProfil = false;
            this.settings = this.defaultSettings = {
                'Accounts':{
                    'Addon':{
                        'Profil-Name':			'Standard',
                        'Version':				version
                    },
                    'Arthoria':{
                        'Spieler-ID':			'',
                        'Spieler-Name':			''
                    },
                    'Forum':{
                        'Foren-Nickname'	:		'',
                        'Foren-Passwort'	:		'',
                    }
                },
                'Sidebar':{
                    'Aktiviert':				true,
                    'Fixiert':					true,
                    'Art':						'vertical',
                    'Position':					'left',
                    'Links':					[]
                },
                'Plugins':{
                    'Forenzugriff':{
                        'Suche aktivieren':			false
                    },
                    'Krondall':{
                        'Aktiviert':				true
                    },
                    'Dandron':{
                        'Aktiviert':				true
                    },
                    'Forschung':{
                        'Aktiviert':				true,
                        'Automatisch absenden':	true
                    },
                    'Herstellung':{
                        'Herstellungsseite erweitern':	false,
                        'Str+Alt+C aktivieren':			true,
                        'Suche aktivieren':				true,
                        'Zutaten Liste erzeugen':		true
                    },
                    'Chat':{
                        'PM Channels':						false,
                        'Nachrichten wiederherstellen':		false,
                        'Nachrichtenliste':					['Temporäres feld'],
                        'Vordefinierte Liste':				[]
                    },
                    'Spielererweiterungen':{
                        'Aktiviert':				false,
                        'Suche aktivieren':			true,
                        'Statusüberwachung':		{
                            'Aktiviert':				true,
                            'list':						[]
                        }
                    },
                    'Itemerweiterungen':{
                        'Suche aktivieren':			true
                    },
                    'Kampferweiterungen':{
                        'Aktiviert':				true,
                        'Voreinstellung':			4,
                        'Gruppeneinstellungen':		{}
                    },
                    'Phasenportal':{
                        'Kämpfe zählen': true,
                        'Kampfzähler': 0,
                        'LetztesKampfDatumAlsTimestamp': 0,
                        'HöchsteGegnerId':0,
                        'Rätsel lösen': true
                    }
                }
            };
            var plugins = [];
// Methodes:
            this.addPlugin = function(plugin) {
                plugins.push(plugin);
            }
            this.getPlugin = function(pluginName) {
                for(var i=0;i<plugins.length;i++) {
                    if(plugins[i].name == pluginName)
                        return plugins[i];
                }
            }
            this.checkForUpdates = function() {
                // Nur alle 12 Stunden testen:
                localStorage.lastUpdate = localStorage.lastUpdate || 0;
                localStorage.updateAvailable = localStorage.updateAvailable || false;
                if(!JSON.parse(localStorage.updateAvailable) && parseInt(localStorage.lastUpdate)+12*60*60*1000 > (new Date()).getTime())
                    return;
                $.getJSON(addonServer+'version.php?callback=?', function(resp) {
                    localStorage.lastUpdate = (new Date()).getTime();
                    if(version < resp.version) {
                        localStorage.updateAvailable = true;
                        $('#Arthoria_QualiDev_Addon_Sidebar > ul').append(
                            $('<li style="border:1px solid black;padding:10px;"></li>').append(
                                $('<a></a>').attr('target', '_blanc').attr('href', addonServer+'arthoria_qualidev_addonV'+resp.version+'.user.js').html('Es gibt eine neue Version des Qualidev Addons. Diese Version funktioniert weiterhin, jedoch wird empfohlen zu updaten.').click(function() {
                                    window.open(addonServer+'updateNotes.html?c='+new Date().getTime());
                                })
                            )
                        );
                    } else {
                        app.setSetting('Version', 'Accounts.Addon', version);
                        localStorage.updateAvailable = false;
                    }
                });
            },
                this.loadSettings = function(callback) {
                    _this = this;
                    // Mögliche Zustände (allgemein):
                    // 1. 	Settings sind im Browser hinterlegt
                    // 1.1		für den User
                    // 1.2		für den falschen User
                    // 2. 	Settings sind nicht im Browser hinterlegt
                    // 2.1		können vom Server geladen werden
                    // 2.2		existieren nicht auf dem Server
                    if(!environment.puzzle) {
                        issetError.call(this, loadLocalSettings, 'CALLELSE', loadRemoteSettings);
                    } else {
                        loadPuzzleSettings();
                    }
                    function loadPuzzleSettings() {
                        this.settings = this.defaultSettings;
                        loadRemoteSettings = loadLocalSettings = loadProfil = newProfil = deleteProfil = _this.setSetting = _this.saveSettings = _this.delSetting = function() {};
                        callback();
                    }
                    // local functions
                    function loadLocalSettings() {
                        this.settings = JSON.parse(localStorage.getItem('settings'));
                        this.cryptedProfil = JSON.parse(localStorage.getItem('crypted'));
                        if(!this.settings || typeof this.settings != 'object') throw "Settings-error: Keine lokalen Einstellungen gespeichert.";
                        var playerId = this.settings.Accounts.Arthoria['Spieler-ID'];
                        if(!playerId || playerId != unsafeWindow.charID) throw "Settings-error: Lokale Einstellungen passen nicht zum angemeldeten Spieler.";
                        this.settings = $.extend(true, {}, this.defaultSettings, this.settings);
                        this.initDone();
                    }
                    function loadRemoteSettings() {
                        this.settings = this.defaultSettings;
                        this.settings.Accounts.Arthoria['Spieler-ID'] = unsafeWindow.charID;
                        var $frm = createModalForm.call(this, loadProfil.bind(this), newProfil.bind(this), deleteProfil.bind(this));
                        $frm.find(':input').attr('disabled', 'disabled');
                        $.ajax({
                            url:addonServer+'manageProfil.php?action=getProfilnames&uid='+unsafeWindow.charID,
                            async:false,
                            dataType:"json"
                        }).done(function(json) {
                            $frm.children('p').hide();
                            $frm.find(':input').removeAttr('disabled');
                            $.each(json.list, function(idx, val) {
                                if(typeof val == 'string')
                                    $frm.find('form > fieldset > select').append($('<option data-encrypted="false">'+val+'</option>'));
                                else
                                    $frm.find('form > fieldset > select').append($('<option data-encrypted="true">'+val.name+'</option>'));
                            });
                        });
                    }
                    function loadProfil(key, pName) {
                        key = key || '';
                        var fullProfilname = unsafeWindow.charID+'_'+pName;
                        $.ajax({
                            url:addonServer+'manageProfil.php?action=get&profil='+fullProfilname+'&key='+CryptoJS.SHA1(key).toString().substr(0,20),
                            async:false,
                            dataType:"json"
                        }).done(function(json) {
                            if(json.success) {
                                self.settings = $.extend(true, {}, self.defaultSettings, JSON.parse(self.decryptProfilData(key, json.profilData)));
                                if(key!='')
                                    self.cryptedProfil = key;
                                else
                                    self.cryptedProfil = false;
                                self.saveSettings();
                                self.initDone();
                            } else {
                                alert('Falsches Passwort.');
                                console.log(json);
                            }
                        });
                    }
                    function newProfil(key, pName) {
                        key = key || '';
                        var othis = this;
                        var fullProfilname = unsafeWindow.charID+'_'+pName;
                        var tmpSettings = this.defaultSettings;
                        tmpSettings.Accounts.Addon['Profil-Name'] = pName;
                        $.ajax({
                            url:addonServer+'manageProfil.php?action=create&profil='+fullProfilname+'&key='+CryptoJS.SHA1(key).toString().substr(0,20),
                            type:"POST",
                            data:{
                                "settings":self.encryptProfilData(key, tmpSettings)
                            },
                            async:false,
                            dataType:"json"
                        }).done(function(json) {
                            if(json.success) {
                                window.location.replace('?p=qualidev_addon');
                            } else {
                                alert('Es trat ein Fehler beim anlegen des Profils auf!');
                            }
                        });
                    }
                    function deleteProfil(key, pName) {
                        key = key || '';
                        var othis = this;
                        var fullProfilname = unsafeWindow.charID+'_'+pName;
                        $.ajax({
                            url:addonServer+'manageProfil.php?action=delete&profil='+fullProfilname+'&key='+CryptoJS.SHA1(key).toString().substr(0,20),
                            type:"POST",
                            async:false,
                            dataType:"json"
                        }).done(function(json) {
                            if(json.success) {
                                localStorage.removeItem('settings');
                                localStorage.removeItem('crypted');
                                self.cryptedProfil = false;
                                window.location.replace('?p=qualidev_addon');
                            } else {
                                alert(json.message);
                            }
                        });
                    }
                    function createModalForm(callbackLoadProfil, callbackNewProfil, callbackDeleteProfil) {
                        var tmpThis = this;
                        var $frm = $('\
			<div id="dialog-form" title="Profilwahl">\
				<p>Loading...</p>\
				<form>\
					<fieldset>\
						<label for="profilname">Profils</label>\
						<select name="profilname" id="profilname" class="select ui-widget-content ui-corner-all"></select>\
					</fieldset>\
				</form>\
			</div>\
			').appendTo($('body'));
//			console.log('$frm created!');
//			console.log('$frm.dialog: '+$frm.dialog);
                        $frm.dialog({
                            autoOpen: true,
                            height: 300,
                            width: 350,
                            modal: true,
                            buttons: {
                                "Profil Laden": function() {
                                    var pName = $('#profilname').val();
                                    var key = null;
                                    if($('#profilname > option:selected').attr('data-encrypted')=="true")
                                        key = prompt('Dieses Profil ist Passwort geschützt. Bitte geben Sie Ihr Passwort für das profil "'+pName+'" ein: ', '');
                                    if (pName) {
                                        $( this ).dialog( "close" );
                                        callbackLoadProfil.call(tmpThis, key, pName);
                                    } else {
                                        $( this ).children('p').show().html('Bitte wähle ein Profil aus oder lege ein neues an.');
                                    }
                                },
                                "Neues Profil anlegen": function() {
                                    var othis = this;
                                    $( this ).find('form > fieldset > br, form > fieldset > label[for=newprofilname], form > fieldset > input:text').remove();
                                    $( this ).find('form > fieldset').append(
                                        $('<br><label for="newprofilname">Neuer Profilname: </label><input type="text" name="newprofilname" id="newprofilname" class="text ui-widget-content ui-corner-all">')
                                    ).append($('<input type="submit" value="Erstellen">').button().click(function(evt) {
                                        evt.preventDefault();
                                        if($('#newprofilname').val().length < 3) {
                                            $(othis).children('p').show().html('Der Name muss mindestens aus 3 Zeichen bestehen.');
                                        } else {
                                            var key = prompt('Bitte geben Sie ein Passwort für das Profil "'+$('#newprofilname').val()+'" ein: ','');
                                            $(othis).dialog('close');
                                            callbackNewProfil.call(tmpThis, key, $('#newprofilname').val());
                                        }
                                    }));
                                },
                                "Profil löschen": function() {
                                    var pName = $('#profilname').val();
                                    var key = null;
                                    if($('#profilname > option:selected').attr('data-encrypted')=='true')
                                        key = prompt('Dieses Profil ist Passwort geschützt. Bitte geben Sie Ihr Passwort für das profil "'+pName+'" ein um dieses zu Löschen: ', '');
                                    if(pName) {
                                        callbackDeleteProfil.call(tmpThis, key, pName);
                                    }
                                }
                            },
                            close: function() {
                                $('#profilname').val( "" );
                            }
                        });
                        return $frm;
                    }
                },
                this.init = function() {
                    // Lade gespeicherte Settings und führe callback aus:
                    addAddonSiteCode(function() {
                        console.log('addAddonSiteCode:: Callbacked');
                        self.loadSettings(self.initDone);
                    });
                }
            this.initDone = function() {
                console.log('initDone');
                self.execute();
                self.checkForUpdates();
            }
            this.encryptProfilData = function(key, rawProfilData) {
                if(typeof rawProfilData != 'string') {
                    rawProfilData = JSON.stringify(rawProfilData);
                }
                if(key!='') {
                    return CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(rawProfilData), CryptoJS.SHA1(key).toString()).toString();
                } else
                    return rawProfilData;
            }
            this.decryptProfilData = function(key, encrpytedProfilData) {
                if(typeof encrpytedProfilData != 'string') {
                    encrpytedProfilData = JSON.stringify(encrpytedProfilData);
                }
                if(key!='')
                    return CryptoJS.AES.decrypt(encrpytedProfilData, CryptoJS.SHA1(key).toString()).toString(CryptoJS.enc.Utf8);
                else
                    return encrpytedProfilData;
            }
            this.setSetting = function(key, branch, val) {
                var node = this.settings;
                var branch = branch.split('.');
                for(var i=0;i<branch.length;i++)
                    node = node[branch[i]];
                if(val===null)
                    if($.isArray(node) && !isNaN(parseFloat(key)) && isFinite(key))
                        node.splice(parseInt(key), 1);
                    else
                        delete node[key];
                else
                    node[key] = val;

                // Update Sidebar if setting changed:
                if(branch[0]=='Sidebar')
                    location.reload();
                this.saveSettings();
            }
            this.saveSettings = function() {
                var profilName = this.settings.Accounts.Arthoria['Spieler-ID']+'_'+this.settings.Accounts.Addon['Profil-Name'];
                var key = '';
                if(self.cryptedProfil)
                    key = self.cryptedProfil;
                tKey = CryptoJS.SHA1(key).toString().substr(0,20);
                $.post(addonServer+'manageProfil.php?action=update&profil='+profilName+'&key='+tKey, {"settings":self.encryptProfilData(key, this.settings)}, function(json) {
                    if(json.success) {
                        localStorage.setItem('settings', JSON.stringify(self.settings));
                        localStorage.setItem('crypted', JSON.stringify(self.cryptedProfil));
                    } else
                        alert('Falsches Passwort.');
                });
            }
            this.delSetting = function(key, branch) {
                this.setSetting(key, branch, null);
            }
            this.execute = function() {
                for(var i=0;i<plugins.length;i++)
                    if(plugins[i])
                        plugins[i].init();
                if(window.location.href.indexOf('?p=qualidev_addon') > -1 && !environment.puzzle) {
                    $.ajax({
                        'url':addonServer+'settings_interface_new.php?v='+version,
                        'async':false,
                        'dataType':'json',
                        'success':function(response) {
                            $('.pad5LR:eq(0)').html(response.key);
                            $('html, body').animate({
                                scrollTop:$('.pad5LR:eq(0)').offset().top
                            }, 1000);
                        }
                    });
                }
                // Settings setzten (auf allen Seiten, auf denen welche eingeblendet werden)
                $('[data-setting]').each(function() {
                    var $this = $(this);
                    if(!$this.attr('data-setting')) return;
                    var parts = $this.attr('data-setting').split('.');
                    var node = self.settings;
                    for(var i=0;i<parts.length;i++)
                        if(typeof node[parts[i]] == 'undefined')
                            return;
                        else
                            node = node[parts[i]];
                    if($this.is(':checkbox')) {
                        if(node)
                            $this.attr('checked', 'checked');
                    } else if($this.is('select')) {
                        $this.children('option[value="'+node+'"]').attr('selected', 'selected');
                    } else if($this.is(':text, :password')) {
                        $this.val(node);
                    } else {
                        $this.text(node);
                    }
                });
                // Settingsänderungen speichern:
                $('.Arthoria_QualiDev_Addon_Settings :checkbox, .Arthoria_QualiDev_Addon_Settings select').change(function() {
                    $this = $(this);
                    var tmp = $this.attr('data-setting').split('.');
                    var key = tmp[tmp.length-1];
                    var branch = tmp.slice(0,-1).join('.');
                    var val = $this.is(':checkbox') ? ($this.is(':checked') ? true : false ) : $this.val() ;
                    self.setSetting(key, branch, val);
                });
                $('.Arthoria_QualiDev_Addon_Settings :text, .Arthoria_QualiDev_Addon_Settings :password').keyup(function() {
                    $this = $(this);
                    var tmp = $this.attr('data-setting').split('.');
                    var key = tmp[tmp.length-1];
                    var branch = tmp.slice(0,-1).join('.');
                    var val = $this.val() ;
                    self.setSetting(key, branch, val);
                });
                $('input:submit[name="callProfilManager"]').click(function(evt) {
                    evt.preventDefault();
                    localStorage.removeItem('settings');
                    window.location.replace('?p=qualidev_addon');
                });
                // Hier alle Kampfgruppen mit Einstellung aufführen:
                for(var groupName in self.settings.Plugins.Kampferweiterungen.Gruppeneinstellungen) {
                    var idFightStyle	= self.settings.Plugins.Kampferweiterungen.Gruppeneinstellungen[groupName];
                    var spellName		= 'Standardeinstellung';
                    if(idFightStyle.spellName) {
                        spellName		= idFightStyle.spellName || spellName;
                        idFightStyle	= idFightStyle.fightStyle;
                    }
                    $('.Arthoria_QualiDev_Addon_Settings h4:contains(Gruppeneinstellungen)').parent().parent().children('div:eq(0)').children('ul').append(
                        $('<li>'+groupName+': '+spellName+'>>'+$('.Arthoria_QualiDev_Addon_Settings select[data-setting="Plugins.Kampferweiterungen.Voreinstellung"]')
                            .find('option[value="'+idFightStyle+'"]')
                            .text()+' | <a data-group="'+groupName+'">del</a> |</li>'
                        )
                    );
                }
                $('.Arthoria_QualiDev_Addon_Settings h4:contains(Gruppeneinstellungen)').parent().parent().children('div:eq(0)').find('ul > li > a').click(function() {
                    self.delSetting($(this).attr('data-group'), 'Plugins.Kampferweiterungen.Gruppeneinstellungen');
                    $(this).parent().remove();
                });
                // Hier alle im Chat gespeicherten Nachrichten aufführen:
                for(var msgId in self.settings.Plugins.Chat['Vordefinierte Liste']) {
                    var message = self.settings.Plugins.Chat['Vordefinierte Liste'][msgId];
                    $('.Arthoria_QualiDev_Addon_Settings h4:contains(Gespeicherte Nachrichten)').parent().parent().children('div:eq(0)').children('ul').append(
                        $('<li><i>'+message+'</i> | <a data-group="'+msgId+'">del</a> |</li>')
                    );
                }
                $('.Arthoria_QualiDev_Addon_Settings h4:contains(Gespeicherte Nachrichten)').parent().parent().children('div:eq(0)').find('ul > li > a').click(function() {
                    self.delSetting($(this).attr('data-group'), 'Plugins.Chat.Vordefinierte Liste');
                    $(this).parent().remove();
                });
            }
            function addAddonSiteCode(callback) {
                addCSSs();
                addScripts();

                function addScripts() {
                    $.ajax({
                        url:'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
                        dataType:'script',
                        cache:true,
                        success:function() {
                            $ = jQuery.noConflict();
                            $.ajax({
                                url:'https://arthoria.srv1-14119.srv-net.de/jquery-ui-1.12.1.custom.js',
                                dataType:'script',
                                cache:true,
                                success:function() {
                                    extendJQuery();
                                    callback();
                                }
                            });
                        }
                    });
                    // Fix für kaefers script, was auf $('script:eq(2)) zugreift, statt einfach auf window.charID :roll:
                    $('script[src]').html('var charID="'+window.charID+'";');
                }
                function extendJQuery() {
                    $.fn.getSelector = function() {
                        var selector = '';
                        paths = this.parents().map(function() {
                            var $this = $(this);
                            var id = $this.attr('id');
                            var idx= $this.parent().children(this.tagName).index($this);
                            return [[this.tagName, id, idx]];
                        });
                        var sStr = [];
                        $.each(paths.get(),function(key, itm) {
                            var tmpSel = itm[0];
                            if(itm[1]) {
                                tmpSel+='#'+itm[1];
                            } else {
                                tmpSel+=':eq('+itm[2]+')';
                            }
                            sStr.push(tmpSel);
                            if(itm[1]) return false;
                        });
                        return sStr.reverse().join(' > ');
                    }
                    $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
                        _title: function(title) {
                            if (!this.options.title ) {
                                title.html("&#160;");
                            } else {
                                title.html(this.options.title);
                            }
                        }
                    }));
                }
                function addCSSs() {
                    var link = document.createElement('link');
                    link.type="text/css";
                    link.rel="stylesheet";
                    link.href='https://arthoria.srv1-14119.srv-net.de/css-jQuery/jquery-ui-1.12.1.custom.css';
                    document.head.appendChild(link);
                    // eigene CSS Geschichten:
                    var style = document.createElement('style');
                    style.type="text/css";
                    style.innerHTML = '#Arthoria_QualiDev_Addon_Sidebar { text-align:center; z-index:201; }';
                    style.innerHTML+='#Arthoria_QualiDev_Addon_Sidebar > ul > li[data]:hover { background-color:rgba(255,255,255,0.2); cursor:pointer; }\n';
                    style.innerHTML+='#Arthoria_QualiDev_Addon_Sidebar > ul > li[data] > a { text-decoration:none; font-weight:normal; }\n';
                    style.innerHTML+='.Arthoria_QualiDev_Addon_Settings div.nav { text-align:center; }\n';
                    style.innerHTML+='.Arthoria_QualiDev_Addon_Settings fieldset { margin-bottom:20px; }\n';
                    style.innerHTML+='.Arthoria_QualiDev_Addon_Settings fieldset label { display:inline-block; width:300px; position:relative; margin:5px auto; }\n';
                    style.innerHTML+='.Arthoria_QualiDev_Addon_Settings fieldset label input, .Arthoria_QualiDev_Addon_Settings fieldset label select { position:absolute;right:0; }\n';
                    style.innerHTML+='fieldset legend h1, fieldset legend h2, fieldset legend h3, fieldset legend h4 { margin:0 10px 10px 10px; }\n';
                    style.innerHTML+='img[data-func] {cursor:pointer;}\n';
                    style.innerHTML+='.ui-dialog {z-index:200;} \n';
                    document.head.appendChild(style);
                }
            }
        }

        /*************************************************************************************\
         |*** Ziel		: Status Page
         |*** Funktion	: Set color to red for unfullfilled tasks
         \*************************************************************************************/
        function StatusPagePlugin() {
            var self = this;
            this.name = 'StatusPagePlugin';
            this.init = function() {
                // experimental, daher nur für mich freigeschaltet
                if(unsafeWindow.charID != '33621')
                    return;

                var $rideTd = $('h3:contains(regelmäßige Beschäftigungen)').next('table').find('> tbody > tr > td:contains(Reittier:)').next('td');
                if($rideTd.text().indexOf('Hufeisen nötig')>-1)
                    $rideTd.append(', ').append($('<a href="?p=stable&action=2&adv=1">verbesserte Hufeisen</a>'));
                $('td:eq(0)', $('h3:contains(regelmäßige Beschäftigungen)').next('table').find('> tbody > tr')).each(function() {
                    var $this = $(this);
                    if(
                        ($.trim($this.text()) == 'Falle im Anwesen:' && $.trim($this.next().text()) == 'nicht vergiftet')
                        ||
                        ($.trim($this.text()) == 'Spruchrolle: Illusionsschlacht:' && $.trim($this.next().text()) != 'bereits gewirkt')
                        ||
                        ($.trim($this.text()) == 'Forschung:' && $.trim($this.next().text()) != 'Forschungseinheit bereits absolviert')
                        ||
                        ($.trim($this.text()) == 'Artefakt:' && $.trim($this.next().text()).substring(0,14) != 'aufgeladen bis')
                        ||
                        ($.trim($this.text()) == 'Auftrag der Stadtverwaltung:' && $.trim($this.next().text()) != 'Auftrag erfüllt')
                    ) {
                        $this.parent().css('background-color', '#CC0000');
                        if($.trim($this.text()) == 'Spruchrolle: Illusionsschlacht:')
                            $('h3:contains(regelmäßige Beschäftigungen)').next('table').find('> tbody > tr > td:contains(Spruchrolle: Illusionsschlacht:)').next('td').append(' (').append($('<a>verstärkt</a>').attr('href','https://arthoria.de/index.php?p=inventory&use=5936')).append(')');
                    }
                });
            }
        }

        /*************************************************************************************\
         |*** Ziel		: Phasenwebsite + Sidebar
         |*** Funktion	: Zählt die Besuche bei den Phasenwesen pro Tag.
         \*************************************************************************************/
        function PhasenportalPlugin() {
            var self = this;
            this.name = 'PhasenportalPlugin';
            var symWidth = 32, symHeight = 32,
                symSpaceX = 16, symSpaceY = 16,
                mainCanvas = document.createElement('canvas'),
                mainContext = mainCanvas.getContext('2d'),
                counts = [0,0,0,0,0,0,0,0,0],
                phasenImage,
                icons,
                order;

            this.init = function() {
                var oldDate = new Date(app.settings.Plugins.Phasenportal.LetztesKampfDatumAlsTimestamp);
                var today = new Date();
                if(oldDate.format('Ymd') < today.format('Ymd')) {
                    app.setSetting('Kampfzähler', 'Plugins.Phasenportal', 0);
                    app.setSetting('LetztesKampfDatumAlsTimestamp', 'Plugins.Phasenportal', today.getTime());
                }
                if(app.settings.Plugins.Phasenportal['Rätsel lösen'] && window.location.href.indexOf('?p=imgriddle') > -1 && !$('div.infobox').filter(function() { return this.innerHTML.indexOf('Das Phasenmuster hat sich noch nicht wieder verändert.')>-1; }).length)
                    this.solveRiddle();
                if(!app.settings.Plugins.Phasenportal['Kämpfe zählen'] || window.location.href.indexOf('?p=battle&portalraid=1&ty=') < 0)
                    return;
                if(!window.firstStepXML)
                    return;
                var $tmpData = $(window.firstStepXML);
                var cid = $tmpData.find('c[group=1]').attr('id');
                if(cid<=app.settings.Plugins.Phasenportal['HöchsteGegnerId'])
                    return;
                app.setSetting('Kampfzähler', 'Plugins.Phasenportal', app.settings.Plugins.Phasenportal['Kampfzähler']+1);
                app.setSetting('HöchsteGegnerId', 'Plugins.Phasenportal', cid);
            }
            this.solveRiddle = function() {
                var _next =()=>{
                    icons = this.getPieces();
                    order = /absteigend|aufsteigend/.exec($('.pad5LR > p:contains("Sortiere die Symbole ")').eq(0).text());

                    mainCanvas.width = phasenImage.width;
                    mainCanvas.height = phasenImage.height;
                    mainContext.drawImage(phasenImage,0,0);
                    try {
                        this.next();
                        $('input.sendbutton').removeAttr('disabled').removeAttr('title')
                    } catch(err) {
                        $('input.sendbutton').attr('disabled', 'disabled').parent().after($('<span>').css('color', 'red').text('Bitte lade die Seite neu! Es ist ein Fehler aufgetreten.'));
                    }
                }

                phasenImage = $('img[src^="graphics/img4p.php?imgid="]').get(0);
                phasenImage.onload = _next;
                if(phasenImage.complete && typeof phasenImage.naturalWidth != "undefined" && phasenImage.naturalWidth > 0) {
                    _next();
                }
            }

            this.getPieces = function getPieces() {
                var ps = [];
                var self = this;
                $('#symrow > a[id^=a] > img').each(function() {
                    var cT = document.createElement('canvas');
                    cT.width = symWidth; cT.height = symHeight;
                    cT.getContext('2d').drawImage(this,0,0);
                    ps.push(self.toHashCode(cT.getContext('2d').getImageData(0,0,symWidth,symHeight).data));
                });
                return ps;
            }
            this.next = function next() {
                // count symbols:
                // console.log('start');
                for(x=0;x<8;x++) {
                    for(y=0;y<6;y++) {
                        // console.log('in');
                        var symPosX = symSpaceX+x*(symWidth+2*symSpaceX);
                        var symPosY = symSpaceY+y*(symHeight+2*symSpaceY);
                        var data = mainContext.getImageData(symPosX,symPosY,symWidth,symHeight);
                        var debug = document.createElement('canvas');
                        debug.width = symWidth;
                        debug.height = symHeight;
                        debug.getContext('2d').putImageData(data, 0, 0);
                        var symStr = this.toHashCode(data.data);
                        // console.log(symStr);
                        var idx = icons.indexOf(symStr);
                        if(idx<0) {
                            throw 'Wurde nicht gefunden!';
                        } else {
                            counts[idx]++;
                        }
                    }
                }
                var map = [];
                for(k in counts)
                    map.push({count:counts[k], key:parseInt(k,10)});
                map.sort(function(a, b) {
                    if(order=='absteigend')
                        return a.count < b.count ? 1 : -1;
                    else
                        return a.count > b.count ? 1 : -1;
                });
                for(i=0;i<map.length;i++) {
                    sym2back(map[i].key+1);
                }
            }
            this.toHashCode = function toHashCode(dArray) {
                var code = 128;
                var m = 257;
                for(i=0;i<dArray.length-3;i+=4) {
                    var fieldValue = Math.floor(dArray[i]/10)*10*255*255 + Math.floor(dArray[i+1]/10)*10*255 + Math.floor(dArray[i+2]/10)*10 + Math.floor(dArray[i+3]/10)*10;
                    if(fieldValue>0) {
                        code = (code * m + fieldValue) % Math.pow(2,32);
                    }
                }
                return code;
            }

        }

        /*************************************************************************************\
         |*** Ziel		: Herstellungsseite
         |*** Funktion	: Alles was mit Rezepturen zu tun hat.
         \*************************************************************************************/
        function AlchimistHelperPlugin() {
            var self = this;
            var data = null;
            var prices = null;
            this.name = 'AlchimistHelperPlugin';

            this.init = function() {
                this.loadAlchimistList();
                if(window.location.href.indexOf('?p=alchemy') > 0)
                    this.addButton();
                if(!app.settings.Plugins.Herstellung['Str+Alt+C aktivieren'] || window.location.href.indexOf('?p=alchemy') < 0)
                    return;
                this.createLearnedRecipes();
            }
            this.addButton = function addButton() {
                $a = $('<a>Ansicht ändern</a>').css('cursor', 'pointer').click(function(evt) {
                    var newStatus = $(this).data('status')=='long' ? 'short' : 'long';
                    $(this).data('status', newStatus);
                    if(newStatus=='short'){
                        $('.pad5LR > table table').hide();
                    } else {
                        $('.pad5LR > table table').show();
                    }
                });
                $a.data('status', 'long');
                $('h1:contains(Verfügbare Rezepturen:)').html('Verfügbare Rezepturen (').append($a).append('):');
            }
            this.calculatePrice = function(itemname) {
                var priceList = self.prices;
                var ingrediens = app.getPlugin('AlchimistHelperPlugin').recursivListing(itemname, {}, 1);
                var sumMin = 0,
                    sumMax = 0,
                    sumMid = 0;
                for(i in ingrediens) {
                    var itmName = i.toLowerCase();
                    if(!priceList[itmName])
                        throw 'Nicht alle Itempreise vorhanden. Es fehlt: '+i;
                    else {
                        sumMin+= ingrediens[i]*priceList[itmName].min;
                        sumMax+= ingrediens[i]*priceList[itmName].max;
                        sumMid+= ingrediens[i]*(parseInt(priceList[itmName].min)+parseInt(priceList[itmName].max))/2;
                    }
                };
                return {min: sumMin, mid: sumMid, max: sumMax};
            }
            this.searchComposition = function(compositionName) {
                if(!self.data) {
                    self.loadAlchimistList();
                    alert('Ein Fehler trat auf. Bitte versuche es in ein paar Sekunden erneut.');
                    return;
                }
                var result = [];
                for(var item in self.data) {
                    if(item.toLowerCase().indexOf(compositionName.toLowerCase()) > -1)
                        result.push(item);
                }
                return result;
            }
            this.createLearnedRecipes = function() {
                $(window).keydown(function(evt) {
                    if(evt.altKey && evt.ctrlKey && evt.keyCode==67)
                        showAlchi();
                });

                function showAlchi() {
                    // Überschrift finden:
                    var ele = $('h1:contains(Verfügbare Rezepturen:)').eq(0);
                    el = ele.parent().children('table:eq(0)');
                    // Gefunden, nun del content:
                    var str = '';
                    var str = el.find('tr').find('a:eq(0)').not(':contains(Create)').map(function() { return $(this).html(); }).get().join('<br>');
                    $('<div title="Liste deiner Rezepturen auf dieser Seite">'+str+'</div>').appendTo($('body')).dialog({
                        height:$(window).height()-200,
                        width:400
                    });
                }
            }
            this.loadAlchimistList = function() {
                if(!localStorage.alchimistList) {
                    $.getJSON('https://arthoria.srv1-14119.srv-net.de/getAlchemistList.php', function(response) {
                        self.data = response;
                        localStorage.setItem('alchimistList', JSON.stringify(self.data));
                    });
                } else {
                    self.data = JSON.parse(localStorage.alchimistList);
                }
                if(!localStorage.priceList) {
                    $.getJSON(addonServer+'../getItemsCost.php', function(obj) {
                        self.prices = obj;
                        localStorage.setItem('priceList', JSON.stringify(obj));
                    });
                } else {
                    self.prices = JSON.parse(localStorage.priceList);
                }
            },
                this.recursivListBuilding = function(val, cnt) {
                    var $pul = $('<ul></ul>');
                    for(key in self.data[val]['ingrediens']) {
                        var iVal = key;
                        var $pli_tag = $('<li></li>').html(cnt*self.data[val]['ingrediens'][key]+'x '+iVal);
                        if(typeof self.data[iVal] != 'undefined') {
                            $pli_tag.append(self.recursivListBuilding(iVal, cnt*self.data[val]['ingrediens'][iVal]));
                        }
                        $pul.append($pli_tag);
                    };
                    return $pul;
                },
                this.recursivListing = function(itemname, pres, cnt) {
                    var tpres = {};
                    var x = self.data[itemname];
                    if(typeof x == 'object') {
                        for(var i in x['ingrediens']) {
                            var pres	= self.recursivListing(i, pres, cnt*x['ingrediens'][i]);
                        }
                    } else {
                        pres[itemname] 	= ((pres[itemname]) ? pres[itemname] : 0 ) + cnt;
                    }
                    return pres;
                }
        }

        /*************************************************************************************\
         |*** Ziel		: Im Kampf
         |*** Funktion	: Erlaubt das setzten von Voreinstellungen, auch für Kampfgruppen einzeln.
         \*************************************************************************************/
        function BattleExtendPlugin() {
            this.name = 'BattleExtendPlugin';

            this.init = function() {
                if(!app.settings.Plugins.Kampferweiterungen.Aktiviert || ($('#auto_l').size()==0 && $('#auto_as').size()==0 && window.location.href.indexOf('?p=battle&') < 0))
                    return;
                if(!unsafeWindow.firstStepXML)
                    return;
                // Auslesen der Kampfgruppen:
                var xml = (new DOMParser()).parseFromString(unsafeWindow.firstStepXML, 'text/xml');
                var groups = [];
                var combs = xml.getElementsByTagName('c');
                for(var i=0;i<combs.length;i++) {
                    var g = combs[i].getAttribute('group');
                    if(!groups[g]) groups[g] = [];
                    groups[g].push(combs[i].getElementsByTagName('n')[0].firstChild.nodeValue);
                }
                groups[1].sort();
                // den kampfgruppencode generieren:
                var code = groups[1].join(',');
                this.readOut(code);
                this.execute(code);
            }
            this.readOut = function(code) {
                /*********************************************************************\
                 |**** 1. Das Auslesen ************************************************|
                 \*********************************************************************/
                $('#auto_l, #auto_as').change(function(evt) {
                    if($('li[data="changeFightpresets"]').size()) {
                        var dataTargetParts = $('li[data="changeFightpresets"]').attr('data-target').split(',');
                        if($.inArray(this.id, dataTargetParts)>-1)
                            return;
                        dataTargetParts.push(this.id);
                        $('li[data="changeFightpresets"]').attr('data-target', dataTargetParts.join(','));
                    } else {
                        $('<li style="border:1px solid black;padding:10px; cursor:pointer;" data="changeFightpresets" data-target="'+this.id+'">Für diese Kampfgruppe merken?</li>').click(function(evt) {
                            var obj = {};
                            var dataTargetParts = $(this).attr('data-target').split(',');
                            for(var i=0;i<dataTargetParts.length;i++) {
                                if(dataTargetParts[i]=='auto_as') {
                                    obj.spellId 	= $('#auto_as').val();
                                    obj.spellName	= $('#auto_as').children('option[value="'+$('#auto_as').val()+'"]').text();
                                } else {
                                    obj.fightStyle	= $('#auto_l').val();
                                }
                            }
                            console.log(obj);
                            if(obj == {})
                                app.delSetting(code, 'Plugins.Kampferweiterungen.Gruppeneinstellungen');
                            else
                                app.setSetting(code, 'Plugins.Kampferweiterungen.Gruppeneinstellungen', obj);
                            $(this).css('background-color', 'green');
                        }).appendTo(
                            $('#Arthoria_QualiDev_Addon_Sidebar > ul')
                        );
                    }
                });
            }
            this.execute = function(code) {
                // Im Kampf entsprechende Voreinstellung setzten:
                var idFightStyle = app.settings.Plugins.Kampferweiterungen.Voreinstellung;
                var idSpell;
                // EXTENDED-AddOn:
                if(unsafeWindow.firstStepXML) {
                    /*********************************************************************\
                     |**** 1. Das Anzeigen ************************************************|
                     \*********************************************************************/
                        // Gibt es einen speziellencode für diese Kampfgruppe?
                    var fightpresetRecord;
                    if(app.settings.Plugins.Kampferweiterungen.Gruppeneinstellungen[code]) {
                        fightpresetRecord = app.settings.Plugins.Kampferweiterungen.Gruppeneinstellungen[code];
                        if(fightpresetRecord.fightStyle)
                            idFightStyle	= fightpresetRecord.fightStyle;
                        else if(typeof fightpresetRecord=='number')
                            idFightStyle	= fightpresetRecord;
                        idSpell			= fightpresetRecord.spellId		|| null;
                    }
                }
                $('#auto_l').val(idFightStyle);
                if(idSpell)
                    $('#auto_as').val(idSpell);
            }
        }

        function ChatPlugin() {
            var stackpointer = 0;
            this.name = 'ChatPlugin';

            this.init = function() {
                // Chat Nachrichten wieder herstellen:
                if(!app.settings.Plugins.Chat['Nachrichten wiederherstellen'])
                    return;

                $('#chatmessage').attr('autocomplete', 'off');
                $('#chatmessage').keydown(function(evt) {
                    var list 	= app.settings.Plugins.Chat.Nachrichtenliste;
                    var dList	= app.defaultSettings.Plugins.Chat.Nachrichtenliste;
                    var preList = app.settings.Plugins.Chat['Vordefinierte Liste'];
                    // Falls die Einstellung irgendwie resettet wurde, setzten wir hier wieder den Tempeintrag
                    if(list.length==0)
                        $.extend(list, dList);
                    // Enter
                    if(evt.keyCode==13) {
                        list[0] = $(this).val();
                        list.unshift(dList[0]);
                        if(list.length>50) {
                            list.pop();
                        }
                        app.setSetting('Nachrichtenliste', 'Plugins.Chat', list);
                    }
                    // Esc
                    if(evt.keyCode==27) {
                        $select = $('<select>');
                        $select.append($('<option>').html('Bitte wählen...'));
                        $.each(preList, function(idx, val) {
                            $select.append($('<option>').html(val));
                        });
                        $select.insertAfter($('#chatmessage'));
                        $select.change(function(evt) {
                            $('#chatmessage').val($(this).val()).focus();
                            $(this).remove();
                        });
                    }
                    // up
                    if(evt.keyCode==38) {
                        evt.preventDefault();
                        if(stackpointer==list.length-1) return;
                        if(stackpointer==0) list[0] = $(this).val();
                        stackpointer++;
                        $('#chatmessage').val(list[stackpointer]);
                    }
                    // down
                    if(evt.keyCode==40) {
                        evt.preventDefault();
                        if(stackpointer==0) return;
                        stackpointer--;
                        $('#chatmessage').val(list[stackpointer]);
                        if(stackpointer==0) list[0] = dList[0];
                    }
                    if(evt.keyCode=='S'.charCodeAt(0) && evt.ctrlKey) {
                        evt.preventDefault();
                        preList.push($(this).val());
                        app.setSetting('Vordefinierte Liste', 'Plugins.Chat', preList);
                    }
                });
            }
            this.freezeChat = function(status) {
                freezed = status;
            }
        }

        /*************************************************************************************\
         |*** Ziel		: Dandronseite
         |*** Funktion	: Zeigt ein Raster an, wo die Spielsteine hingehören
         \*************************************************************************************/
        function DandronSolverPlugin() {
            this.parts = [];
            this.name = 'DandronSolverPlugin';

            this.init = function() {
                if(!app.settings.Plugins.Dandron.Aktiviert || window.location.href.indexOf('?p=puzzle')	< 0)
                    return;
                // Teile indizieren:
                this.helper.indiziere();
                // Alle Teile durchgehen:
                var c = 0;
                while(document.getElementById("QD_Dandron_Spielstein_" + c)) {
                    this.parts[c] = this.helper.getPuzzleToken($("#QD_Dandron_Spielstein_" + c));
                    c++;
                }
                if(this.parts.join('').replace(/\n/g,'').replace(/0/g,'').length == 64) {
                    // Es ist im Startzustand!
                    this.helper.loadSolve(this.parts);
                } else if(localStorage.getItem('dandron_response')) {
                    this.helper.showIt(localStorage.getItem('dandron_response'));
                } else {
                    alert('Lösen nicht möglich. Nehmen Sie bitte alle Steine vom Spielfeld. Danke.');
                }
            }
            this.helper = {
                'loadSolveOld':function(parts) {
                    // Das der richtige Input!!
                    var self = this;
                    var my_input = parts.join("\n,\n");
                    $.ajax({
                        'url':'https://arthoria.srv1-14119.srv-net.de/proxy.php?callback=?',
                        'data':{
                            'value':my_input,
                            'url':'http://www.dandron-loeser.de/ajax/dandron'
                        },
                        'dataType':'jsonp',
                        'jsonp':'jsonp_callback',
                        'success':function(response) {
                            text = response.key.replace(/'/g, '"').replace(/\r\n/g, '').replace(/\n/g,'').replace(/\t/g,'').replace(/  /g,' ').replace(/&bull;/g,'').replace(/&nbsp;/g,'');
                            localStorage.setItem('dandron_response', text);
                            self.showIt(text);
                        },
                        'error':function(response) {
                            alert(response);
                        }
                    });
                },
                'loadSolve':function(parts) {
                    var sWTs = [],
                        size = 8,
                        self = this;

                    for(i=0;i<parts.length;i++) {
                        (function() {
                            var sWT = new DandronSolverPlugin.SingleWorkerThread(size, parts);
                            sWT.onmessage = function(event) {
                                if(event.data.type == 'solution') {
                                    sWTs.forEach(s => s.terminate());
                                    var text = self.createTableText(event.data.solution);
                                    if(text) {
                                        localStorage.setItem('dandron_response', text);
                                        self.showIt(text);
                                    }
                                }
                            }
                            sWTs.push(sWT);
                        })();
                    }
                },
                'createTableText':function(solution) {
                    var $table = $('<table>'),
                        $tr;
                    var translate = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'];
                    solution.board.forEach((cls, idx) => {
                        if(idx%8==0) $tr = $('<tr>');
                        $tr.append($('<td><span class="'+['bull', 'd'+translate[cls-1]].join(' ')+'"></span></td>'));
                        if(idx%8==7) $table.append($tr);
                    });
                    return $table.wrap('<div>').parent().html();
                },
                'showIt':function(text) {
                    // Anzeigen:
                    var eles = $('td', $('table[background*="graphics/puzzle/field.png"]'));
                    var eles2= $('<div></div>').html(text).find('span');
                    eles.each(function(index) {
                        $(this).attr('class', eles2.eq(index).removeClass('bull').attr('class'));
                    });
                    $('<style/>').attr("type", "text/css").html('\
			.d1 {background-color: #000000;}/*schwarz*/ \
			.d2 {background-color: #FF0000;}/*rot*/ \
			.d3 {background-color: #0000FF;}/*blau*/ \
			.d4 {background-color: #347C2C;}/*Spring green4*/ \
			.d5 {background-color: #01b901;}/*gr?n*/ \
			.d6 {background-color: #800000;}/*weinrot*/ \
			.d7 {background-color: #FF00CC;}/*pink*/ \
			.d8 {background-color: #00FFF0;}/*t?rkis*/ \
			.d9 {background-color: #D2691E;}/*anders braun*/ \
			.da {background-color: #696969;}/*grau*/ \
			.db {background-color: #736AFF;}/*light slate blue*/ \
			.dc {background-color: #999999;}/*rosa*/ \
			.dd {background-color: #FFFF00;}/*gelb*/ \
			.de {background-color: #6600FF;}/*dunkles lila*/ \
			.df {background-color: #AAFF00;}/*gelbgr?n*/ \
			.dg {background-color: #AA0033;}/*dunkelrot*/ \
			.dh {background-color: #FF9900;}/*orange*/ \
			.di {background-color: #FFCC66;}/*helles gelb*/').appendTo(document.head);
                    var self = this;
                    function f(evt) {
                        evt.preventDefault();
                        $.get($(this).attr('href'), function(res) {
                            $pad5LR = $('<div></div>').html(res).find('.pad5LR');
                            // update board:
                            var $tds = $pad5LR.children('table').eq(0).find('tbody > tr > td:eq(0) > table > tbody > tr > td');

                            eles.each(function(idx) {
                                var $this = $(this);
                                $this.removeAttr('background');
                                $this.attr('background', $tds.eq(idx).attr('background'));
                                $this.children('a').attr('href', $tds.eq(idx).children('a').attr('href'));

                                $this.find('a > img')
                                    .attr('src', $tds.eq(idx).find('a > img').attr('src'))
                                    .unbind('mouseover').unbind('mouseout')
                                    .removeAttr('onMouseOver').removeAttr('onMouseOut')
                                    .bind('mouseover', function() {
                                        $tds.eq(idx).find('a > img').mouseover();
                                    })
                                    .bind('mouseout', function() {
                                        $tds.eq(idx).find('a > img').mouseout();
                                    });
                            });

                            //console.log($pad5LR.children('table').eq(0).find('tbody > tr > td:eq(1)').html());
                            $('.pad5LR').children('table').eq(0).find('> tbody > tr > td:eq(1)').html($pad5LR.children('table').eq(0).find('> tbody > tr > td:eq(1)').html());
                            $('.pad5LR > div:not(.infobox)[style*="background.jpg"]:eq(0)').html($pad5LR.children('div:not(.infobox)[style*="background.jpg"]:eq(0)').html());
                            self.indiziere();

                            if($('.infobox:contains(Lotterielos)', $pad5LR).length) {
                                alert($('.infobox:contains(Lotterielos)', $pad5LR).html().replace(/<br\s*\/?>/gi,'\n'));
                            }
                        });
                    }

                    $(document).on('click', 'div[id^="QD_Dandron_Spielstein_"] a, table[background*="graphics/puzzle/field.png"] a', f);
                },
                'indiziere':function() {
                    // Dandron Spielsteine:
                    var el = $('div').filter(function(index) { return $(this).css('background-image').indexOf('graphics/puzzle/background.jpg')>-1; }).eq(0);
                    if(el.length) {
                        var cppp = 0;
                        $a = el.children('div').filter(function(index) {
                            return $(this).find('a').attr('href').indexOf('index.php?p=puzzle&pick=') > -1;
                        }).each(function(index) {
                            $(this).attr('id', "QD_Dandron_Spielstein_" + cppp++);
                        });
                    }
                },
                'getPuzzleToken':function(element) {
                    var tmpElemente = element.children("div"), result = [];

                    var kugel_breite	= parseInt(tmpElemente.eq(0).css('width'));
                    var kugel_hoehe		= parseInt(tmpElemente.eq(0).css('height'));

                    tmpElemente.each(function(index) {
                        var px = parseInt($(this).css('left')) / kugel_breite;
                        var py = parseInt($(this).css('top')) / kugel_hoehe;
                        if(typeof result[py] == 'undefined')
                            result[py] = [];
                        result[py][px] = 1;
                    });

                    $.each(result, function(index) {
                        $.each(result[index], function(innerindex) {
                            if(result[index][innerindex] != 1)
                                result[index][innerindex] = 0;
                        });
                        result[index] = result[index].join('');
                    });
                    result = result.join("\n");
                    return result;
                },
                'getWorkerJSCode':function() {
                    return function() {
                        function Piece(bin, no) {
                            this.bin = bin;
                            this.no = no;
                        }
                        Piece.prototype.bin;
                        Piece.prototype.no;

                        var _size;
                        var _number;
                        self.onmessage = function(data) {
                            var o = JSON.parse(data.data);
                            _size = o.size;
                            _number = o.number;

                            var solution = new Solve();
                            solution.pieces = Solve.prepareParts(o.pieces);
                            try {
                                solution.solve();
                            } catch(err) {
                                self.postMessage({type:'solution', workerThreadNumber:_number, solution:err});
                            }
                            self.postMessage({type:'ende'});
                        }

                        function Solve() {
                            this.board = Array(_size*_size).fill(0); // Zeilen in eine Zeile
                        }
                        Solve.prototype.pieces;
                        Solve.prototype.board;
                        Solve.prototype.solve = function() {
                            var idx = _number;
                            this.setPiece(0, this.pieces.slice(idx, idx+1)[0]);
                            this.solveNextStep(this.pieces.slice(0,idx).concat(this.pieces.slice(idx+1)), [idx]);
                        }
                        Solve.prototype.solveNextStep = function solveNextStep(tail, chain) {
                            if(!tail.length) throw {chain:chain, board:this.board};
                            chain = chain || [];
                            var that = this;
                            var nextFree = this.nextFree();
                            tail.forEach(function(piece, idx) {
                                if(success = that.setPiece(nextFree, piece)) {
                                    that.solveNextStep(tail.slice(0,idx).concat(tail.slice(idx+1)), chain.concat([idx]));
                                    that.delPiece(nextFree, piece);
                                }
                            });
                        }
                        Solve.prototype.nextFree = function() {
                            return this.board.indexOf(0);
                        }
                        Solve.prototype.setPiece = function setPiece(posInBoard, piece) {
                            var i=0;
                            while(piece.bin[i++]==0&&posInBoard>0) posInBoard--;
                            if(piece.bin.lastIndexOf(1)+posInBoard>_size*_size-1) return false;
                            var success = true,
                                tmpBoard = this.board.slice();
                            var l=0;
                            for(i=posInBoard;i<_size*_size;i++) {
                                if((tmpBoard[i] > 0 && piece.bin[i-posInBoard]>0) || (piece.bin[i-posInBoard]>0 && i%8<l)) {
                                    success = false;
                                    break;
                                }
                                tmpBoard[i] = tmpBoard[i] + piece.bin[i-posInBoard] * piece.no;
                                l=piece.bin[i-posInBoard]==0?0:i%8;
                            }
                            if(success) this.board = tmpBoard.slice();
                            return success;
                        }
                        Solve.prototype.delPiece = function delPiece(posInBoard, piece) {
                            var i=0;
                            while(piece.bin[i++]==0&&posInBoard>0) posInBoard--;
                            for(i=posInBoard;i<_size*_size;i++)
                                this.board[i] = piece.bin[i-posInBoard] ? 0 : this.board[i];
                        }
                        Solve.prepareParts = function(parts) {
                            return Object.keys(parts).map(idx => {
                                var piece = parts[idx];
                                var ret = new Array(_size*_size).fill(0);
                                piece.split(/\n/).forEach((lP, lNo) => {
                                    lP.split('').forEach((rP, rNo) => {
                                        var newPos= lNo*_size + rNo;
                                        ret[newPos] = rP*1;
                                    });
                                });
                                return new Piece(ret, parseInt(idx,10)+1);
                            });
                        }
                    };
                }
            }
        }
        DandronSolverPlugin.SingleWorkerThread = function SingleWorkerThread(_size, parts) {
            var _myNumber = SingleWorkerThread.currentNumber++;
            var _worker = SingleWorkerThread.createFromCodeString(app.getPlugin('DandronSolverPlugin').helper.getWorkerJSCode().toString());
            _worker.postMessage(JSON.stringify({
                size:_size,
                pieces:parts,
                number:_myNumber
            }));
            return _worker;
        }
        DandronSolverPlugin.SingleWorkerThread.currentNumber = 0;
        DandronSolverPlugin.SingleWorkerThread.createFromCodeString = function(code) {
            var blob = new Blob(["("+code+")()"], {type: 'text/javascript'});

            return new Worker(window.URL.createObjectURL(blob));
        }


        /*************************************************************************************\
         |*** Ziel		: Überall
         |*** Funktion	: Erlaubt das durchsuchen des Forums
         \*************************************************************************************/
        function ForenAccessPlugin() {
            this.name = 'ForenAccessPlugin';

            this.init = function() {}
            this.forenSearch = function(needle) {
                var result = [];
                if(needle.length >= 3) {
                    $.ajax({
                        url:addonServer+'proxy.php',
                        async:false,
                        data:{
                            url:'http://arthoria-forum.de/index.php?action=login2',
                            data:'user=***&passwrd=***&cookielength=***&hash_passwrd=***'
                        },
                        success:function(resp1) {
                            $.ajax({
                                url:addonServer+'proxy.php',
                                async:false,
                                data:{
                                    url:'http://arthoria-forum.de/index.php?action=search2',
                                    data:'search='+needle+'&submit=Suche',
                                    cookies:'PHPSESSID='+resp1.cookies.PHPSESSID+';'
                                },
                                success:function(resp) {
                                    var ele = $('<div></div>').html(resp.body).find('#main_content_section').find('.search_results_posts');
                                    alert(ele.size());
                                    ele.each(function() {
                                        var $this = $(this);
                                        result.push({
                                            "href":$this.find('.topic_details.floatleft > h5 > a').last().attr('href'),
                                            "name":$this.find('.topic_details.floatleft > h5 > a').last().text()
                                        });
                                    });
                                }
                            });
                        }
                    });
                }
                return result;
            }
        }

        /*************************************************************************************\
         |*** Ziel		: Verschiedenes
         |*** Funktion	: Bietet die Itemsuche an und macht noch anderen kram (später)
         \*************************************************************************************/
        function ItemExtensionPlugin() {
            this.name = 'ItemExtensionPlugin';
            this.init = function() {
                if(window.location.href.indexOf('p=item') > -1) {
                    this.insertWikiLink();
                }
            },
                this.itemSearch = function(needle) {
                    var result = [];
                    if(needle.length >= 3) {
                        $.ajax({
                            url:'index.php?p=market&cr=1&type=2&duration=1&special=1&g1=1&item1='+needle,
                            async:false,
                            beforeSend : function(xhr) {
                                xhr.setRequestHeader('Content-Type', "text/html;charset=ISO-8859-1");
                            },
                            success:function(resp) {
                                var ele = $('<div></div>').html(resp).find('.pad5LR').find('select[name=itemid] > option');
                                ele.each(function() {
                                    var $this = $(this);
                                    result.push({
                                        "id":$this.val(),
                                        "name":$this.text()
                                    });
                                });
                            }
                        });
                    }
                    return result;
                }
            this.insertWikiLink = function() {
                var $u       = $('h1:contains(Gegenstand)').next('table').find('> tbody > tr:eq(0) > td:eq(0) > u'),
                    itemName = $u.text();
                if(/stab/.test(itemName)) {
                    // Auf normalnamen kürzen:
                    itemName = itemName.replace(/^.*?(\S*stab\S*).*$/, '$1');
                }

                $('<a href="http://arthoria.wikia.com/wiki/'+itemName+'" target="_blank" style="display:inline-block;margin-left:10px;"><img src="" alt="" />[W]</a>').insertAfter($u);
            }
            /* wird nicht verwendet. Zu viel Aufwand
          this.checkIfWikiEntryExists = function(itemName) {
              return $.ajax({
                  url:'index.php?p=market&cr=1&type=2&duration=1&special=1&g1=1&item1='+needle,
                    async:false,
                    beforeSend : function(xhr) {
                      xhr.setRequestHeader('Content-Type', "text/html;charset=ISO-8859-1");
                    },
                    success:function(resp) {
                      var ele = $('<div></div>').html(resp).find('.pad5LR').find('select[name=itemid] > option');
                        ele.each(function() {
                          var $this = $(this);
                            result.push({
                              "id":$this.val(),
                                "name":$this.text()
                            });
                        });
                    }
                });
            }
            */
            this.getItemTitleAndTableFromPageCode = function(el) {
                return {
                    title: $(el).find('.pad5LR > table:eq(0) u:eq(0)').text(),
                    table: $(el).find('.pad5LR > table:eq(0) tr:eq(0) > td:eq(0) > table:eq(0)')
                }
            }
        }

        /*************************************************************************************\
         |*** Ziel		: Krondallseite
         |*** Funktion	: Löst das Krondall instant
         \*************************************************************************************/
        function KrondallSolverPlugin() {
            var self = this;
            this.crystals = [];
            this.name = 'KrondallSolverPlugin';

            this.init = function() {
                if(!app.settings.Plugins.Krondall.Aktiviert || window.location.href.indexOf('?p=krondall')	< 0)
                    return;
                // find the script
                var code = /var links\s*=.*\s*\);/gim.exec($('h1:contains(Krondall):eq(0)').siblings('script').eq(0).html());
                if(code && code.length > 0 && code[0].length > 0)
                    $('#sol').val(this.helper.solve(code[0].replace(/[\r\n]/g, '')).join('/')).parents('form').submit();
            }
            this.helper = {
                'swap':function(arr, a, b) { var h=arr[a]; arr[a]=arr[b]; arr[b]=h; return arr; },
                'solve':function(line) {
                    eval(line);
                    var posis = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
                    var change = true;
                    while(change) {
                        change = false;
                        for(var i = posis.length-1; i >= 0; i--) {
                            var c = links[posis[i]];
                            for(var j = 0; j < c.length; j++) {
                                var c2 = $.inArray(c[j], posis);
                                if(c2>-1 && c2 <  i) {
                                    posis = self.helper.swap(posis, c2, i);
                                    change = true;
                                    break;
                                }
                            }
                            if(change)
                                break;
                        }
                    }
                    return posis;
                }
            }
        }

        /*************************************************************************************\
         |*** Ziel		: Spielerseiten (Profilseiten)
         |*** Funktion	: Zeigt auch Nicht-Prem-Spielern alle Links an und Erweitert um Angriff und Gildeneinladung
         \*************************************************************************************/
        function PlayerExtendPlugin() {
            var self = this;
            var playerWatchList = [];
            var $listObject = null;
            this.name = 'PlayerExtendPlugin';

            this.init = function() {
                if(app.settings.Plugins.Spielererweiterungen.Statusüberwachung.Aktiviert) {
                    playerWatchList = app.settings.Plugins.Spielererweiterungen.Statusüberwachung.list;
                    this.showWatchList();
                }
                if(!app.settings.Plugins.Spielererweiterungen.Aktiviert || window.location.href.indexOf('?p=showprofile&i=') < 0)
                    return;
                var $obj = $('td:contains(Charaktername)').size()==2 ? $('td:contains(Charaktername)').eq(1).siblings('td').eq(0) : $('td:contains(Charaktername)').eq(0).siblings('td').eq(0) ;
                var id		= /&i=(\d*)/i.exec(window.location.href)[1];
                // User ohne Profil abfangen (Xeri, System, etc)
                try {
                    var name	= /<img[^>]*>[^A-Za-z0-9]*([^(]*)/i.exec($obj.html())[1];
                } catch(ex) {
                    return;
                }
                // (Nachricht schreiben / Markthalle / FH wirken / TE wirken)
                var msg		= '?p=message&action=1&rec='+id;
                var attack= '?p=attack&e='+id;
                var maha	= '?p=market&cat=0&owner='+id;
                var tavern= '?p=tavern&invpl='+id+'#inplayer';
                var fh		= '?p=inventory&use=226&fhid='+id;
                var te		= '?p=inventory&use=222&teid='+id;
                // Einladenlink:
                var invite	= '?p=guild&page=action&inviteMember='+name;
                // DOM manipulieren:
                var bracketString = '<a href="'+msg+'">Nachricht schreiben</a> / <a href="'+attack+'">Überfallen</a> / <a href="'+tavern+'">Taverne</a> / <a href="'+maha+'">Markthalle</a> / <a href="'+fh+'">FH wirken</a> / <a href="'+te+'">TE wirken</a> / <a href="'+invite+'">Gildeneinladung</a>';
                $obj.html(
                    // Klammer ersetzten:
                    $obj.html().replace(/\(.*\)/i, '')+' ('+bracketString+')'
                );
            }
            this.addPlayerToWatchList = function(id, name) {
                playerWatchList.push({
                    id:		id,
                    name:	name,
                    status:	status,
                    date:	new Date()
                });
                playerWatchList.sort(function(a,b) {return a.name>b.name?1:-1});
                app.setSetting('list', 'Plugins.Spielererweiterungen.Statusüberwachung', playerWatchList);
            }
            this.showWatchList = function() {
                $('<div><h4>Auf der Watchlist</h4><div><ul></ul></div></div>').appendTo($('body'))
                    .dialog({
                        position:{my:"left top", at:"left top", of:window},
                        maxWidth:100,
                        autoOpen:false
                    });
            }
            this.playerSearch = function(needle) {
                var result;
                $.ajax({
                    'url':'https://arthoria.de/index.php',
                    'type':'POST',
                    'data':'p=playerlist&page=search&playername='+needle,
                    'async':false,
                    'success':function(response) {
                        result = $('<div></div>').html(response).find('.pad5LR').find('.boardtable');
                    }
                });
                return result;
            }
        }

        /*************************************************************************************\
         |*** Ziel		: Forschungsseite
         |*** Funktion	: Füllt das Eingabefeld direkt mit der richtigen Zahl
         \*************************************************************************************/
        function ResearchSolverPlugin() {
            this.name = 'ResearchSolverPlugin';

            this.init = function() {
                if(!app.settings.Plugins.Forschung.Aktiviert || window.location.href.indexOf('?p=library&page=research') < 0)
                    return;
                // Die Zahl steht in einem Span, daher suchen wir einfach alle Spans
                // myObj bekommt dann das zutreffende Tag zugewiesen
                // Hiermit wird getestet, ob das span eine lange zahl enth?lt
                // Das span finden:
                var myObj = this.helper.getSpan($("span"), /\w{60,110}/gi);
                // wurde die Zahl localisiert?
                if(myObj.length) {
                    // Die Lösung finden:
                    var tmp_res = this.helper.find_number(myObj.text());
                    // Finden wir das Eingabefeld?
                    var inp = $('input[name=page][value=research]').parents('form').find('input[name=solution]');
                    if(inp.length) inp.val(tmp_res);
                    if(!app.settings.Plugins.Forschung['Automatisch absenden'])
                        return;
                    // Absenden
                    inp.parents('form').submit();
                }
            }
            this.helper = {
                'getSpan':function getSpan($spans, Myexp) {
                    return $spans.filter(function(index) {
                        return $(this).text().length >= 60 && Myexp.test($(this).text());
                    });
                },
                'find_number':function(zahl) {
                    var pos=foundZ=0, len=zahl.length, found=false, sear="", cnt=[];

                    while(!found && len > 0) {
                        len--;
                        cnt = [];
                        for(var pos = 0; pos < zahl.length - len + 1; pos++) {
                            sear = zahl.substr(pos, len);
                            if(sear[0]==='0') continue;
                            for(var i = 0; i < zahl.length - len - pos; i++) {
                                if(zahl.substr(i + pos + 1, len) == sear) {
                                    var tmp = existInCnt(cnt, sear);
                                    if(tmp == cnt.length)
                                        cnt[tmp] = [sear, 1];
                                    cnt[tmp][0] = sear;
                                    cnt[tmp][1] = (!cnt[tmp][1]) ? 1 : cnt[tmp][1]+1 ;
                                }
                            }
                        }
                        foundZ = 0;
                        for(var i = 0; i < cnt.length; i++)
                            if(cnt[i][1] > 1 && foundZ < parseInt(cnt[i][0])) {
                                foundZ = cnt[i][0];
                                found = true;
                            }
                        if(foundZ > 0)
                            break;
                    }
                    if(foundZ) return foundZ;
                    else return "gibbets net?? oO";

                    function existInCnt(cnt, sear) {
                        var fnd = -1;
                        for(var i = 0; i < cnt.length; i++)
                            if(cnt[i][0] == sear)
                                fnd = i;
                        fnd = (fnd == -1) ? cnt.length : fnd;
                        return fnd;
                    }
                }
            }
        }

        /*************************************************************************************\
         |*** Ziel		: Überall
         |*** Funktion	: Blendet überall eine Sidebar mit Zusatzfunktionen ein
         \*************************************************************************************/
        function SidebarPlugin() {
// Variables:
            var self = this;
            var hrefLinks = [];
            var sidebarObject = null;
            var $showcase = null;
            this.name = 'SidebarPlugin';
// Methodes:
            this.init = function() {
                this.update(true);
            }
            this.addLink = function(pos, text, href) {
                hrefLinks[pos] = {
                    text:text,
                    href:href
                };
                this.update();
            }
            this.update = function(create) {
                create = create || false;
                if(!sidebarObject && !create) return; // ist nötig, weil sonst durch die Links versucht wird, die Bar aufzubauen, bevor $j zur verfügung steht
                if(sidebarObject)
                    sidebarObject.remove();
                addCode();
                setEvents();
            }
            this.search = function(needle) {
                var result = {'alchemy':[], 'players':[], 'items':[], 'forumentries':[]};
                if(app.settings.Plugins.Herstellung['Suche aktivieren'])
                    result.alchemy	= app.getPlugin('AlchimistHelperPlugin').searchComposition(needle);
                if(app.settings.Plugins.Spielererweiterungen['Suche aktivieren'])
                    result.players	= app.getPlugin('PlayerExtendPlugin').playerSearch(needle).find('tr');
                if(app.settings.Plugins.Itemerweiterungen['Suche aktivieren'])
                    result.items	= app.getPlugin('ItemExtensionPlugin').itemSearch(needle);
                /*
                if(app.settings.Plugins.Forenzugriff['Suche aktivieren'])
                    result.forumentries = app.getPlugin('ForenAccessPlugin').forenSearch(needle);
                */
                result.items.sort(function(a,b) { return a.name>b.name?1:-1; });
                // Suche wurde gestartet und nun muss das Result angezeigt werden:
                var bgi = $('#page-content').size() ? $('#page-content').css('background-image'):'';
                var bgc = $('#page-content').size()==0 ? '#334433':'transparent';

                if($showcase)
                    $showcase.remove();
                $showcase =
                    $('<div id="arthoria_QD_addon__IngridiensSearch_createlist_temp_result" style="position:fixed;top:50px;right:50px;bottom:50px;left:50px;z-index:200;"></div>')
                        .html('<div style="border-bottom:1px solid black;height:30px;">\
			<img src="'+addonServer+'images/close.png" onclick="$(this).parent().parent().remove();" style="margin:3px;float:right;">\
			<div style="clear:both;"></div>\
		</div>\
		<div style="position:absolute;top:31px;right:0;bottom:0;left:0;padding:0 20px 20px 20px;overflow:auto;">\
			<div data-role="accordion">'
                            +((!app.settings.Plugins.Herstellung['Suche aktivieren']) ?'': '\
				<h3 style="text-align:center;">Rezepturen</h3>\
				<div style="font-size:1em;">rez</div>\
			')
                            +((!app.settings.Plugins.Spielererweiterungen['Suche aktivieren']) ?'': '\
				<h3 style="text-align:center;">Spieler</h3>\
				<div>player</div>\
			')
                            +((!app.settings.Plugins.Itemerweiterungen['Suche aktivieren']) ?'': '\
				<h3 style="text-align:center;">Items</h3>\
				<div>item</div>\
			')
                            +((!app.settings.Plugins.Forenzugriff['Suche aktivieren']) ?'': '\
				<h3 style="text-align:center;">Forenbeiträge</h3>\
				<div>item</div>\
			')
                            +'\
			</div>\
		</div>')
                        .css('background-size', '100%').css('background-image', bgi)
                        .css('background-color', bgc).css('border', '1px solid black')
                        .appendTo(document.body);
                $showcase.children('div:eq(0)').css('background-size', '100%').css('background-image', bgi).css('background-color', bgc);
                $showcase.find('div > div > div').css('text-align', 'left');
                $showcase.find('div > div > h3:contains(Rezepturen)').html('Rezepturen ('+result.alchemy.length+')');
                $showcase.find('div > div > h3:contains(Spieler)').html('Spieler ('+(result.players.length-1)+')');
                $showcase.find('div > div > h3:contains(Items)').html('Items ('+result.items.length+')');
                $showcase.find('div > div > h3:contains(Forenbeiträge)').html('Forenbeiträge ('+result.forumentries.length+')');

                // Rezepturen einfügen:
                if(result.alchemy.length) {
                    $ul = $('<ul></ul>');
                    $.each(result.alchemy, function(key, val) {
                        $('<li></li>').html(val+' <a class="arthoria_QD_addon__IngridiensSearch_createlist_links" dataRName="'+val+'">CreateList</a> <a class="arthoria_QD_addon__IngridiensSearch_calculateprice_links" dataRName="'+val+'">Calculate Price</a>').appendTo($ul);
                    });
                    var $alchemyString = $ul;
                } else {
                    var $alchemyString = $('<span></span>').html('Keine Rezepturen gefunden.');
                }
                $alchemyString = $('<div><a>Berechne Preise</a><br></div>').append($alchemyString);
                // Anhängen:
                $showcase.find('div > div > h3:contains(Rezepturen)').next().html($alchemyString);

                // Spieler einfügen:
                if(result.players.length) {
                    var playerString = $('<table class="boardtable" style="width:100%;"></table>').append(result.players);
                } else {
                    var playerString = 'Keine Spieler gefunden.';
                }
                // Anhängen:
                $showcase.find('div > div > h3:contains(Spieler)').next().html(playerString);

                // Items einfügen:
                if(result.items.length) {
                    $ul = $('<ul></ul>');
                    $.each(result.items, function(key, val) {
                        var $link = $('<a style="text-decoration:bold;">'+val.name+'</a>').click(function(evt) {
                            evt.preventDefault();
                            $.get('index.php', {"p":'item', "i":val.id}, function(resp) {
                                var parts = app.getPlugin('ItemExtensionPlugin').getItemTitleAndTableFromPageCode(resp);
                                $('<div><div>').html(parts.table).dialog({
                                    width:600,
                                    title:'<a href="index.php?p=item&i='+val.id+'">'+parts.title+'</a>'
                                });
                            });
                        });
                        $('<li></li>').html($link).appendTo($ul);
                    });
                    var itemString = $ul;
                } else {
                    var itemString = 'Keine Items gefunden.';
                }
                // Anhängen:
                $showcase.find('div > div > h3:contains(Items)').next().html(itemString);

                // Forenbeiträge einfügen:
                if(result.forumentries.length) {
                    $ul = $('<ul></ul>');
                    $.each(result.forumentries, function(key, val) {
                        var $link = $('<a style="text-decoration:bold;">'+val.name+'</a>').attr('target','_blanc').attr('href', val.href);
                        $('<li></li>').html($link).appendTo($ul);
                    });
                    var forumentriesString = $ul;
                } else {
                    var forumentriesString = 'Keine Foreneinträge gefunden.';
                }
                // Anhängen:
                $showcase.find('div > div > h3:contains(Forenbeiträge)').next().html(forumentriesString);

                // Das Panel öffnen, welches am weitesten oben ist und mehr als 0 Einträge hat:
                var active = false,
                    i = 0;
                for(let entryKey in result) {
                    if((result[entryKey].length && entryKey!=='players') || (result[entryKey].length>1 && entryKey==='players')) {
                        active = i;
                        break;
                    }
                    i++;
                }
//    alert(active);
                $showcase.find('div[data-role=accordion]').accordion({
                    //heightStyle:"content",
                    heightStyle: "fill",
                    collapsible:true,
                    active:active
                });

                // setup links:
                (function() {
                    // Alle Links bei Click etwas tun lassen:
                    $('#arthoria_QD_addon__IngridiensSearch_createlist_temp_result').find('.arthoria_QD_addon__IngridiensSearch_createlist_links').click(function() {
                        var itemname = $.trim($(this).attr('dataRName'));
                        var ingrediens = app.getPlugin('AlchimistHelperPlugin').recursivListing(itemname, {}, 1);
                        var ingListSplitted = app.getPlugin('AlchimistHelperPlugin').data[itemname]['ingrediens'];
                        var $ul = $('<ul style="list-style:none;"></ul>');
                        $.each(ingListSplitted, function(key, val) {
                            val = val+'x '+key;
                            var $li_tag = $('<li></li>').html(val);
                            if(typeof app.getPlugin('AlchimistHelperPlugin').data[val.split(/\d*x /)[1]] != 'undefined') {
                                var tmpVal = /(\d*)x (.*)/.exec(val).slice(1);
                                $li_tag.append(app.getPlugin('AlchimistHelperPlugin').recursivListBuilding(tmpVal[1], tmpVal[0]));
                            }
                            $ul.append($li_tag);
                        });
                        $nul = $('<ul></ul>');
                        for(var key in ingrediens)
                            $nul.append($('<li></li>').html(ingrediens[key]+'x '+key));
                        var $dia = $('<div title="'+itemname+'" style="display:inline-block"></div>')
                            .html($ul.wrap('<div></div>').parent().html()+'<br><hr><br>Alles in allem:'+$nul.wrap('<div></div>').parent().html())
                            .css({'text-align':'left'}).appendTo($('body'));
                        $dia.dialog({
                            height:$(window).height()-200,
                            width:$dia.width()+80
                        });
                    }).parent().find('.arthoria_QD_addon__IngridiensSearch_calculateprice_links').click(function(evt) {
                        try {
                            var price = app.getPlugin('AlchimistHelperPlugin').calculatePrice($.trim($(this).attr('dataRName')));
                            alert(
                                'minimaler Preis: '+Math.ceil(price.min)+' (Maha Gebühren: '+Math.ceil(price.min*1.03+4)+')\n'+
                                'mittlerer Preis: '+Math.ceil(price.mid)+' (Maha Gebühren: '+Math.ceil(price.mid*1.03+4)+')\n'+
                                'maximaler Preis: '+Math.ceil(price.max)+' (Maha Gebühren: '+Math.ceil(price.max*1.03+4)+')'
                            );
                        } catch(msg) {
                            alert(msg);
                        }
                    });
                    $('a:contains(Berechne Preise)').click(function() {
                        $('<div class="arthoria_QD_addon__IngridiensSearch_overlay" style="background-color:white;z-index:10000;position:fixed;top:0;right:0;bottom:0;left:0;"><h1>Bitte Gegenstände durch Komma getrennt eingeben</h1><button>Fertig</button><button>Schließen</button><br>Preis: <label><input type="radio" checked="checked" name="priceType[]" value="min">Minimal</label> - <label><input type="radio" name="priceType[]" value="mid">Mittelweg</label> - <label><input type="radio" name="priceType[]" value="max">Maximal</label><br><label><input type="radio" name="priceWith[]" value="without" checked="checked">Materialpreis</label> - <label><input type="radio" name="priceWith[]" value="with">Mit Gebühren</label><br><textarea style="width:100%;height:80%;"></textarea></div>')
                            .find('button:eq(0)').button().click(function() {
                            if($.trim($(this).parent().children('textarea').val())=='') {
                                alert('Keine Eingabe erfolgt.');
                                return;
                            }
                            var calc = $('input[name="priceWith[]"]:checked').val()=='with'? function(val) {return Math.ceil(val/0.97+4);} : function(val) {return val};
                            var items = $(this).parent().children('textarea').val().replace('\r\n', '\n').replace('\n', '').split(',');
                            items = $.map(items, function(val) {
                                return $.trim(val);
                            });
                            var str = '';
                            for(key in items) {
                                if(items[key]=='')
                                    continue;
                                try {
                                    str+= items[key]+': '+calc(app.getPlugin('AlchimistHelperPlugin').calculatePrice(items[key])[$('input[name="priceType[]"]:checked').val()])+'\n';
                                } catch(msg) {
                                    str+= items[key]+': '+msg+'\n';
                                }
                            }
                            $('.arthoria_QD_addon__IngridiensSearch_overlay textarea').val(str);
                        }).parent()
                            .find('button:eq(1)').button().click(function() {
                            $(this).parent('.arthoria_QD_addon__IngridiensSearch_overlay').remove();
                        })
                            .closest('.arthoria_QD_addon__IngridiensSearch_overlay')
                            .appendTo($('body'));
                    });
                })();
            }
            function addCode() {
                var bgi = $('#page-content').size() ? $('#page-content').css('background-image'):'';
                var bgc = $('#page-content').size()==0 ? '#334433':'transparent';
                let position = app.settings.Sidebar.Position;
                let borderCSS = app.settings.Sidebar.Position=='left'?'border-top-right-radius:10px;':'border-top-left-radius:10px;';

                sidebarObject = $('<div id="Arthoria_QualiDev_Addon_Sidebar" style="position:fixed;top:40%;'+borderCSS+'">\
			<ul style="list-style:none;padding:0;margin-bottom:0;border-bottom:1px solid black;width:150px;">\
				<li style="border:1px solid black;padding:10px;" data="settings">Einstellungen</li>\
				<li style="border:1px solid black;padding:10px;">\
					Suche:<br><input style="width:100px;"><br>\
					<div class="Arthoria_QualiDev_Addon_Settings">\
						<label title="Rezepte durchsuchen" style="float:left;">R: <input type="checkbox" data-setting="Plugins.Herstellung.Suche aktivieren"></label>\
						<label title="Spieler durchsuchen" style="float:left;">S: <input type="checkbox" data-setting="Plugins.Spielererweiterungen.Suche aktivieren"></label>\
						<label title="Items durchsuchen" style="float:left;">I: <input type="checkbox" data-setting="Plugins.Itemerweiterungen.Suche aktivieren"></label>\
						<label title="Forum durchsuchen (disabled)" style="float:left;color:grey;">F: <input type="checkbox" data-setting="Plugins.Forenzugriff.Suche aktivieren" disabled="disabled"></label>\
					</div>\
					<div style="clear:both;"></div>\
				</li>\
				<li style="border:1px solid black;"><u><b>Chat (PNs)</b></u><ul style="padding:0;list-style:none;text-align:left;margin-left:5px;" id="Arthoria_QualiDev_Addon_Sidebar_pns">\
				</ul></li>\
        <li style="border:0px solid black;padding:10px;display:'+(app.settings.Plugins.Phasenportal['Kämpfe zählen']?'block':'none')+'"><a href="?p=guild&page=portal">Phasenkämpfe: <span data-setting="Plugins.Phasenportal.Kampfzähler"></span>/10</a></li>\
			</ul>\
			<div class="Arthoria_QualiDev_Addon_Settings">\
				<span style="float:right;">Fixiert: <input type="checkbox" data-setting="Sidebar.Fixiert"></span>\
			</div>\
		</div>').css('background-position', 'top, center').css('background-image', bgi).css('background-color', bgc).css(position, '-100px').appendTo(document.body);
                for(var i=0;i<hrefLinks.length;i++)
                    if(hrefLinks[i])
                        sidebarObject.children('ul').children('li:eq(2)').after($('<li style="border:1px solid black;padding:10px;"></li>').attr('data', i).html(hrefLinks[i].text));
            }
            function setEvents() {
                addMoveEvents(app.settings.Sidebar.Fixiert, sidebarObject);
                addClickEvents();
                addKeydownSearchEvent(sidebarObject);

                function addKeydownSearchEvent(sidebarObject) {
                    sidebarObject.find('ul > li:eq(1) > input').unbind('keydown');
                    sidebarObject.find('ul > li:eq(1) > input').keydown(function(evt) {
                        if(evt.keyCode == 13) {
                            self.search($(this).val());
                        }
                    });
                }
                function addClickEvents() {
                    sidebarObject.children('ul').off('click', 'li[data]');
                    sidebarObject.children('ul').on('click', 'li[data]', function() {
                        var data = $(this).attr('data');
                        if(!data) return;
                        // static
                        if(data == 'settings') {
                            window.location.href = '?p=qualidev_addon';
                            return;
                        }
                        // Variable
                        if(typeof hrefLinks[data] != 'undefined')
                            window.location.href = hrefLinks[data].href;
                    });
                }
                function addMoveEvents(fixed, sidebarObject) {
                    if(fixed) {
                        sidebarObject.css(app.settings.Sidebar.Position, 0).unbind('mouseenter').unbind('mouseleave');
                    } else {
                        sidebarObject.mouseenter(function() {
                            $(this).animate({
                                [app.settings.Sidebar.Position]:0
                            }, 200);
                        }).mouseleave(function() {
                            $(this).animate({
                                [app.settings.Sidebar.Position]:-(parseInt(sidebarObject.width())-20)
                            }, 200);
                        });
                        sidebarObject.css(app.settings.Sidebar.Position, -(parseInt(sidebarObject.width())-20));
                    }
                }
            }
        }

        function WikiPlugin() {
            var self = this;
            this.name = "WikiPlugin";

            this.init = function() {
                /*
                        this.startWorkOnServer();
                        $j("body").on("click", 'img[data-func]', function(evt) {
                            window.open($(this).attr('data-href'));
                        }).on("keyup", function(evt) {
                            if(evt.ctrlKey && evt.altKey && evt.keyCode == 'W'.charCodeAt(0)) {
                                if(!window.getSelection()) return;
                                var text = window.getSelection();
                                self.insertWikiLink(text, prompt('Bitte trage unten den Link zum Wiki-Artikel von "'+text+'" ein:', ''));
                            };
                        }).on("mouseenter", 'img[data-func]', function(evt) {
                            $('[data-func="wikiLinkHighlight"][data-name="'+$(this).attr('data-name')+'"]').css('color', 'red');
                        }).on("mouseleave", 'img[data-func]', function(evt) {
                            $('[data-func="wikiLinkHighlight"][data-name="'+$(this).attr('data-name')+'"]').css('color', '');
                        });
                */
            }
            this.insertWikiLink = function(word, link) {
                if(!link) return;
                $.ajax({
                    url:addonServer+'manageWikiLinks.php?action=insert&word='+word+'&link='+link,
                    type:'GET',
                    success:function(resp) {
                        // alert(resp);
                    },
                    error:function(a) {
                        alert("Fehler: "+a.error);
                    }
                });
            }
            this.receiveMessage = function(e) {
                if(e.data.body) {
                    var $wikiTags = $('<div></div>').html(e.data.body.replace(/<script.*>.*<\/script>/gi,''));
                    $('#pageframe').html($wikiTags.find('#pageframe'));
                    /*
                                $wikiTags.find('img[data-func="wikiLink"]').each(function() {
                                    $($j(this).getSelector()).html($wikiTags.find($j(this).getSelector()).clone());
                                });
                    */
                } else {
                    console.log(e.data);
                }
            }
            this.startWorkOnServer = function() {
                $.ajax({
                    url:'https://arthoria.srv1-14119.srv-net.de/arthoria_qualidev_addon/WikiPluginWorker.php',
                    type:'POST',
                    data:{
                        sitecode:$('body').html()
                    },
                    success:function(resp) {
                        self.receiveMessage({data:{body:resp}});
                    },
                    error:function(e) {alert(e.error);}
                });
            }
        }


// #################################### HELPER ####################################
        /*************************************************************************************\
         |*** Ziel		: Überall
         |*** Funktion	: führt eine funktion aus und regelt evtl Fehlermeldungen
         \*************************************************************************************/
        function issetError(func, verhalten) {
            verhalten = verhalten || 'IGNORE';
            // IGNORE		<<< Fehler ignorieren, einfach zurück, bei Fehler
            // THROW		<<< Wirft den Fehler weiter hoch
            // RETURN		<<< Fehler zurückgeben
            // CALLELSE		<<< Alternativfunktion bei Fehler aufrufen
            try {
                func.call(this);
                return false;
            }catch(e) {
                switch(verhalten) {
                    case 'CALLELSE':
                        var f = Array.prototype.slice.call(arguments)[2];
                        var e2 = issetError.call(this, f.bind(this, e), 'RETURN');
                        if(e2)
                            return e2;
                        else
                            return false;
                    case 'THROW':
                        throw e;
                    case 'RETURN':
                        return e;
                    case 'IGNORE':
                    default:
                        return true;
                }
            }
            throw "Unknown error!";
        }
    }
    window.eval('window.aqa_addon_runtime = '+String(gmFixWrapper));
    window.eval('window.aqa_addon_runtime()');
})();